function logout() {
    localStorage.setItem('token', null)
    window.location.replace("/login.html")
}