const token = localStorage.getItem('token');

if (token === 'null') {
    // teu acan login
    window.location.replace("/login.html");
} else {
    const postData = {
        token: localStorage.getItem('token')
    };

    $.ajax({
        url: '/verify',
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(postData),
        success: function (res) {
            if (res.message !== 'Authenticated') {
                // Token na expired
                window.location.replace("/login.html")
            }
        },
        error: function (res) {
            console.log(res.responseText);
        }
    })
}