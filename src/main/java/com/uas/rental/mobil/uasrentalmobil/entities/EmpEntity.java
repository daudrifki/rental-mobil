package com.uas.rental.mobil.uasrentalmobil.entities;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Table(name = "emp")
public class EmpEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String emp_name;
    private String nik;
    private String sex;
    private String mobile_nr;
    private String addr_str;
    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmp_name() {
        return emp_name;
    }

    public void setEmp_name(String emp_name) {
        this.emp_name = emp_name;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getMobile_nr() {
        return mobile_nr;
    }

    public void setMobile_nr(String mobile_nr) {
        this.mobile_nr = mobile_nr;
    }

    public String getAddr_str() {
        return addr_str;
    }

    public void setAddr_str(String addr_str) {
        this.addr_str = addr_str;
    }

    @Override
    public String toString() {
        return "EmpEntity{" +
                "id=" + id +
                ", emp_name='" + emp_name + '\'' +
                ", nik='" + nik + '\'' +
                ", sex='" + sex + '\'' +
                ", mobile_nr='" + mobile_nr + '\'' +
                ", addr_str='" + addr_str + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
