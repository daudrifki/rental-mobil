package com.uas.rental.mobil.uasrentalmobil.controller;

import com.uas.rental.mobil.uasrentalmobil.entities.CusEntity;
import com.uas.rental.mobil.uasrentalmobil.repositories.CusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "customer")
public class CusController {
    @Autowired
    CusRepository cusRepository;

    @GetMapping(value  = "checkAPI")
    public String checkAPI(){
        return "Hello World";
    }

    @PostMapping(value = "addNewCus")
    @ResponseBody
    public CusEntity addNewCus(@RequestBody CusEntity param){
        cusRepository.save(param);
        return param;
    }

    @GetMapping(value = "getAllCus")
    public List<CusEntity> getAllCus() {
        return cusRepository.findAll();
    }

    @GetMapping(value = "getById")
    public CusEntity getById(@RequestParam int id){
        return  cusRepository.findById(id).get();
    }

    @PostMapping(value = "updateCus")
    public CusEntity  updateCus(@RequestBody CusEntity param){
        return cusRepository.save(param);
    }

    @GetMapping(value = "deleteCus")
    public List<CusEntity> deleteCus(@RequestParam int id){

        cusRepository.deleteById(id);

        List<CusEntity> cusList = cusRepository.findAll();

        return cusList;
    }
}
