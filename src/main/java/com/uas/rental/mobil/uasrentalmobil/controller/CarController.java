package com.uas.rental.mobil.uasrentalmobil.controller;

import com.uas.rental.mobil.uasrentalmobil.entities.CarEntity;
import com.uas.rental.mobil.uasrentalmobil.repositories.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "car")
public class CarController {

    @Autowired
    CarRepository carRepository;

    @GetMapping(value  = "checkAPI")
    public String checkAPI(){
        return "Hello World";
    }

    @PostMapping(value = "addNewCar")
    @ResponseBody
    public CarEntity addNewCar(@RequestBody CarEntity param){
        carRepository.save(param);
        return param;
    }

    @PostMapping(value = "addTransDetail")
    public List<CarEntity> addTransDetail(@RequestBody List<CarEntity> param){
        carRepository.saveAll(param);
        return param;
    }

    @GetMapping(value = "getAllCar")
    public List<CarEntity> getAllCar() {
        return carRepository.findAll();
    }

    @GetMapping(value = "getById")
    public CarEntity getById(@RequestParam int id){
        return  carRepository.findById(id).get();
    }

    @PostMapping(value = "updateCar")
    public CarEntity  updateCar(@RequestBody CarEntity param){
        return carRepository.save(param);
    }

    @GetMapping(value = "deleteCar")
    public List<CarEntity> deleteCar(@RequestParam int id){

        carRepository.deleteById(id);

        List<CarEntity> carList = carRepository.findAll();

        return carList;
    }

}
