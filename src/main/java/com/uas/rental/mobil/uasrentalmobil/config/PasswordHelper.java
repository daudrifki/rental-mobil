package com.uas.rental.mobil.uasrentalmobil.config;

import at.favre.lib.crypto.bcrypt.BCrypt;

import java.util.Arrays;

public class PasswordHelper {
    public static String hashPassword(String password) {
        return BCrypt.withDefaults().hashToString(12, password.toCharArray());
    }

    public static boolean comparePassword(String raw, String hashed){
        BCrypt.Result result =  BCrypt.verifyer().verify(raw.toCharArray(), hashed);
        return result.verified;
    }
}
