package com.uas.rental.mobil.uasrentalmobil.entities;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Table(name = "customer")
public class CusEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String cus_name;
    private String nik;
    private String sex;
    private String mobile_nr;
    private String addr_str;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getMobile_nr() {
        return mobile_nr;
    }

    public void setMobile_nr(String mobile_nr) {
        this.mobile_nr = mobile_nr;
    }

    public String getAddr_str() {
        return addr_str;
    }

    public void setAddr_str(String addr_str) {
        this.addr_str = addr_str;
    }

    @Override
    public String toString() {
        return "CusEntity{" +
                "id=" + id +
                ", cus_name='" + cus_name + '\'' +
                ", nik='" + nik + '\'' +
                ", sex='" + sex + '\'' +
                ", mobile_nr='" + mobile_nr + '\'' +
                ", addr_str='" + addr_str + '\'' +
                '}';
    }
}
