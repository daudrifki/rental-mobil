package com.uas.rental.mobil.uasrentalmobil.controller;

import at.favre.lib.crypto.bcrypt.BCrypt;
import com.uas.rental.mobil.uasrentalmobil.config.EncryptionHelper;
import com.uas.rental.mobil.uasrentalmobil.config.JwtTokenUtil;
import com.uas.rental.mobil.uasrentalmobil.config.PasswordHelper;
import com.uas.rental.mobil.uasrentalmobil.entities.EmpEntity;
import com.uas.rental.mobil.uasrentalmobil.model.LoginModel;
import com.uas.rental.mobil.uasrentalmobil.model.TokenModel;
import com.uas.rental.mobil.uasrentalmobil.repositories.EmpRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

@RestController
public class AuthController {

    @Autowired
    EmpRepository empRepository;

    @PostMapping("/login")
    @ResponseBody
    public Map login(@RequestBody LoginModel loginModel) {
        EmpEntity emp = empRepository.getEmpByUsername(loginModel.getUsername());

        if (emp == null) {
            // user na teu kapanggih
            return Collections.singletonMap("message", "User Not Found");
        }

        if (!PasswordHelper.comparePassword(loginModel.getPassword(), emp.getPassword())) {
            // password na teu sarua jeung nu di db
            return Collections.singletonMap("message", "Not Authenticated");
        }

        JwtTokenUtil jwtTokenUtil = new JwtTokenUtil();
        String token = jwtTokenUtil.generateToken(emp);

        return Collections.singletonMap("token", token);
    }

    @PostMapping("/verify")
    @ResponseBody
    public Map verifyToken(@RequestBody TokenModel tokenModel) {
        System.out.println(tokenModel.getToken());
        if (tokenModel.getToken().equals("null")) {
            return Collections.singletonMap("message", "Not Authenticated");
        }

        JwtTokenUtil jwtTokenUtil = new JwtTokenUtil();
        String decodedToken = EncryptionHelper.decrypt(tokenModel.getToken());
        String username = jwtTokenUtil.getUsernameFromToken(decodedToken);

        EmpEntity emp = empRepository.getEmpByUsername(username);

        if (jwtTokenUtil.validateToken(decodedToken, emp)) {
            return Collections.singletonMap("message", "Authenticated");
        } else {
            return Collections.singletonMap("message", "Not Authenticated");
        }
    }
}
