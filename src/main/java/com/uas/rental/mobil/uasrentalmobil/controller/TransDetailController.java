package com.uas.rental.mobil.uasrentalmobil.controller;

import com.uas.rental.mobil.uasrentalmobil.entities.TransDetailEntity;
import com.uas.rental.mobil.uasrentalmobil.repositories.TransDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "transaksi_detail")
public class TransDetailController {

    @Autowired
    TransDetailRepository transDetailRepository;

    @GetMapping(value  = "checkAPI")
    public String checkAPI(){
        return "Hello World";
    }

    @PostMapping(value = "addNewTransDetail")
    @ResponseBody
    public TransDetailEntity addNewTransDetail(@RequestBody TransDetailEntity param){
        transDetailRepository.save(param);
        return param;
    }

    @PostMapping(value = "addTransDetailDetail")
    public List<TransDetailEntity> addTransDetailDetail(@RequestBody List<TransDetailEntity> param){
        transDetailRepository.saveAll(param);
        return param;
    }

    @GetMapping(value = "getAllTransDetail")
    public List<TransDetailEntity> getAllTransDetail() {
        return transDetailRepository.findAll();
    }

    @GetMapping(value = "getById")
    public TransDetailEntity getById(@RequestParam int id){
        return  transDetailRepository.findById(id).get();
    }

    @PostMapping(value = "updateTransDetail")
    public TransDetailEntity  updateTransDetail(@RequestBody TransDetailEntity param){
        return transDetailRepository.save(param);
    }

    @GetMapping(value = "deleteTransDetail")
    public List<TransDetailEntity> deleteTransDetail(@RequestParam int id){

        transDetailRepository.deleteById(id);

        List<TransDetailEntity> transDetailList = transDetailRepository.findAll();

        return transDetailList;
    }

}
