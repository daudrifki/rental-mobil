package com.uas.rental.mobil.uasrentalmobil;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UasRentalMobilApplication {

	public static void main(String[] args) {
		SpringApplication.run(UasRentalMobilApplication.class, args);
	}

}
