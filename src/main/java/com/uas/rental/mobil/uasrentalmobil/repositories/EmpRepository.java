package com.uas.rental.mobil.uasrentalmobil.repositories;

import com.uas.rental.mobil.uasrentalmobil.entities.EmpEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface EmpRepository extends JpaRepository<EmpEntity, Integer> {
    @Query(value = "SELECT * FROM emp WHERE username = ?", nativeQuery = true)
    EmpEntity getEmpByUsername(String username);
}
