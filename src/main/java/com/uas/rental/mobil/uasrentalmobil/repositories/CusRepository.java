package com.uas.rental.mobil.uasrentalmobil.repositories;

import com.uas.rental.mobil.uasrentalmobil.entities.CusEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CusRepository extends JpaRepository<CusEntity, Integer> {
}
