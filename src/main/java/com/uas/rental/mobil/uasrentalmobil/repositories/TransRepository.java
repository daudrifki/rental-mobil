package com.uas.rental.mobil.uasrentalmobil.repositories;

import com.uas.rental.mobil.uasrentalmobil.entities.TransEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransRepository extends JpaRepository<TransEntity, Integer> {
}
