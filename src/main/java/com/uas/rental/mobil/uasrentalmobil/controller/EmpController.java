package com.uas.rental.mobil.uasrentalmobil.controller;

import com.uas.rental.mobil.uasrentalmobil.entities.EmpEntity;
import com.uas.rental.mobil.uasrentalmobil.repositories.EmpRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "emp")
public class EmpController {
    @Autowired
    EmpRepository empRepository;

    @GetMapping(value  = "checkAPI")
    public String checkAPI(){
        return "Hello World";
    }

    @PostMapping(value = "addNewEmp")
    @ResponseBody
    public EmpEntity addNewEmp(@RequestBody EmpEntity param){
        empRepository.save(param);
        return param;
    }

    @GetMapping(value = "getAllEmp")
    public List<EmpEntity> getAllEmp() {
        return empRepository.findAll();
    }

    @GetMapping(value = "getById")
    public EmpEntity getById(@RequestParam int id){
        return  empRepository.findById(id).get();
    }

    @PostMapping(value = "updateEmp")
    public EmpEntity  updateEmp(@RequestBody EmpEntity param){
        return empRepository.save(param);
    }

    @GetMapping(value = "deleteEmp")
    public List<EmpEntity> deleteEmp(@RequestParam int id){

        empRepository.deleteById(id);

        List<EmpEntity> empList = empRepository.findAll();

        return empList;
    }
}
