package com.uas.rental.mobil.uasrentalmobil.model;

public class TokenModel {
    private String token;

    public TokenModel() {}

    public TokenModel(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
