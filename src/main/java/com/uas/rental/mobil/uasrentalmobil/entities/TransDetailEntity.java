package com.uas.rental.mobil.uasrentalmobil.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "transaksi_detail")
public class TransDetailEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private int id_trans;
    private String car_name;
    private Date trans_date;
    private int qty;
    private int harga;
    private int jumlah;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_trans() {
        return id_trans;
    }

    public void setId_trans(int id_trans) {
        this.id_trans = id_trans;
    }

    public String getCar_name() {
        return car_name;
    }

    public void setCar_name(String car_name) {
        this.car_name = car_name;
    }

    public Date getTrans_date() {
        return trans_date;
    }

    public void setTrans_date(Date trans_date) {
        this.trans_date = trans_date;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }

    @Override
    public String toString() {
        return "TransDetailEntity{" +
                "id=" + id +
                ", id_trans=" + id_trans +
                ", car_name='" + car_name + '\'' +
                ", trans_date=" + trans_date +
                ", qty=" + qty +
                ", harga=" + harga +
                ", jumlah=" + jumlah +
                '}';
    }
}
