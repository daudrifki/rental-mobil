package com.uas.rental.mobil.uasrentalmobil.repositories;

import com.uas.rental.mobil.uasrentalmobil.entities.CarEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarRepository extends JpaRepository<CarEntity, Integer> {
}
