package com.uas.rental.mobil.uasrentalmobil.controller;

import com.uas.rental.mobil.uasrentalmobil.entities.TransEntity;
import com.uas.rental.mobil.uasrentalmobil.repositories.TransRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "transaksi")
public class TransController {

    @Autowired
    TransRepository transRepository;

    @GetMapping(value  = "checkAPI")
    public String checkAPI(){
        return "Hello World";
    }

    @PostMapping(value = "addNewTrans")
    @ResponseBody
    public TransEntity addNewTrans(@RequestBody TransEntity param){
        transRepository.save(param);
        return transRepository.save(param);
    }

    @PostMapping(value = "addTransDetail")
    public List<TransEntity> addTransDetail(@RequestBody List<TransEntity> param){
        transRepository.saveAll(param);
        return param;
    }

    @GetMapping(value = "getAllTrans")
    public List<TransEntity> getAllTrans() {
        return transRepository.findAll();
    }

    @GetMapping(value = "getById")
    public TransEntity getById(@RequestParam int id){
        return  transRepository.findById(id).get();
    }

    @PostMapping(value = "updateTrans")
    public TransEntity  updateTrans(@RequestBody TransEntity param){
        return transRepository.save(param);
    }

    @GetMapping(value = "deleteTrans")
    public List<TransEntity> deleteTrans(@RequestParam int id){

        transRepository.deleteById(id);

        List<TransEntity> transList = transRepository.findAll();

        return transList;
    }

}
