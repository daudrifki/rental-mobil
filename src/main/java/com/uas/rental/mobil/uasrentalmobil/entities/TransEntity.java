package com.uas.rental.mobil.uasrentalmobil.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "transaksi")
public class TransEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String trans_name;
    private String cus_name;
    private String emp_name;
    private Date trans_date;
    private int total_qty;
    private int total_harga;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTrans_name() {
        return trans_name;
    }

    public void setTrans_name(String trans_name) {
        this.trans_name = trans_name;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getEmp_name() {
        return emp_name;
    }

    public void setEmp_name(String emp_name) {
        this.emp_name = emp_name;
    }

    public Date getTrans_date() {
        return trans_date;
    }

    public void setTrans_date(Date trans_date) {
        this.trans_date = trans_date;
    }

    public int getTotal_qty() {
        return total_qty;
    }

    public void setTotal_qty(int total_qty) {
        this.total_qty = total_qty;
    }

    public int getTotal_harga() {
        return total_harga;
    }

    public void setTotal_harga(int total_harga) {
        this.total_harga = total_harga;
    }

    @Override
    public String toString() {
        return "TransEntity{" +
                "id=" + id +
                ", trans_name='" + trans_name + '\'' +
                ", cus_name='" + cus_name + '\'' +
                ", emp_name='" + emp_name + '\'' +
                ", trans_date=" + trans_date +
                ", total_qty=" + total_qty +
                ", total_harga=" + total_harga +
                '}';
    }
}
