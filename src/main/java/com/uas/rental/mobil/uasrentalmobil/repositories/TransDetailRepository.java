package com.uas.rental.mobil.uasrentalmobil.repositories;

import com.uas.rental.mobil.uasrentalmobil.entities.TransDetailEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransDetailRepository extends JpaRepository<TransDetailEntity, Integer> {
}
