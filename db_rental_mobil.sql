-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 08, 2021 at 05:26 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_rental_mobil`
--

-- --------------------------------------------------------

--
-- Table structure for table `car`
--

CREATE TABLE `car` (
  `id` int(11) NOT NULL,
  `car_name` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `number` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `harga` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `car`
--

INSERT INTO `car` (`id`, `car_name`, `color`, `number`, `type`, `harga`) VALUES
(2, 'Xpander', 'Hitam', 'B 3443 GX', 'AT', 100000),
(3, 'Avanza', 'Hitam', 'D 1122 SS', 'MT', 200000),
(4, 'Xenia', 'Hitam', 'D 5345 FG', 'AT', 300000),
(8, 'Freed', 'Putih', 'D 1294 DF', 'MT', 400000),
(9, 'Pajero', 'Putih', 'D 5342 YG', 'MT', 500000),
(15, 'Hiace Mantap', 'Putih', 'B 8524 GX', 'AT', 600000),
(20, 'Inova', 'Putih', 'D 1635 AX', 'MT', 700000);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `cus_name` varchar(255) NOT NULL,
  `nik` varchar(255) NOT NULL,
  `sex` varchar(255) NOT NULL,
  `mobile_nr` varchar(255) NOT NULL,
  `addr_str` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `cus_name`, `nik`, `sex`, `mobile_nr`, `addr_str`) VALUES
(18, 'Jhon WIck', '19231239123', 'Laki-Laki', '087123123', 'New York');

-- --------------------------------------------------------

--
-- Table structure for table `emp`
--

CREATE TABLE `emp` (
  `id` int(11) NOT NULL,
  `emp_name` varchar(255) NOT NULL,
  `nik` varchar(255) NOT NULL,
  `sex` varchar(20) NOT NULL,
  `mobile_nr` varchar(255) NOT NULL,
  `addr_str` varchar(255) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `emp`
--

INSERT INTO `emp` (`id`, `emp_name`, `nik`, `sex`, `mobile_nr`, `addr_str`, `username`, `password`) VALUES
(16, 'Asep', '100001', 'Laki-Laki', '08123123123', 'Bandung', 'sep', 'punyaasep'),
(17, 'Nina Bobo', '100002', 'Perempuan', '08241314121', 'Bogor', 'nina', '$2a$12$.osvWqMQr/7yN3JAln/JfOLxoZs91ifm8P44NUJ5eLMGGkWgeCZ0G');

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(25);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id` int(11) NOT NULL,
  `trans_name` varchar(255) NOT NULL,
  `cus_name` varchar(255) NOT NULL,
  `emp_name` varchar(255) NOT NULL,
  `trans_date` date NOT NULL,
  `total_qty` int(20) NOT NULL,
  `total_harga` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id`, `trans_name`, `cus_name`, `emp_name`, `trans_date`, `total_qty`, `total_harga`) VALUES
(22, 'tes', 'Jhon WIck', 'Asep', '2021-07-04', 3, 500000),
(23, 'aef', 'Jhon WIck', 'Asep', '2021-07-04', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_detail`
--

CREATE TABLE `transaksi_detail` (
  `id` int(11) NOT NULL,
  `id_trans` int(11) NOT NULL,
  `car_name` varchar(255) NOT NULL,
  `trans_date` date NOT NULL,
  `qty` int(20) NOT NULL,
  `harga` int(20) NOT NULL,
  `jumlah` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `car`
--
ALTER TABLE `car`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emp`
--
ALTER TABLE `emp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi_detail`
--
ALTER TABLE `transaksi_detail`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `emp`
--
ALTER TABLE `emp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `transaksi_detail`
--
ALTER TABLE `transaksi_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
